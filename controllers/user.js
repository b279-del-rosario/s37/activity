const User = require('../models/User');


//Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: reqBody.password
	})


	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})

};

// activity
module.exports.userDetails = (profileID) =>{
	return User.findById(profileId).then((result) =>{
		if(result == null){
			return false;
		}else {
			result.password = ""
			
			return result
		}
	})
}